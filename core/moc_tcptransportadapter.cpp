/****************************************************************************
** Meta object code from reading C++ file 'tcptransportadapter.h'
**
** Created: Sat Jun 6 17:23:53 2015
**      by: The Qt Meta Object Compiler version 63 (Qt 4.8.2)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "transport/tcp/tcptransportadapter.h"
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'tcptransportadapter.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 63
#error "This file was generated using the moc from 4.8.2. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
static const uint qt_meta_data_letilib__TcpTransportAdapter[] = {

 // content:
       6,       // revision
       0,       // classname
       0,    0, // classinfo
       4,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       2,       // signalCount

 // signals: signature, parameters, type, tag, flags
      29,   53,   61,   61, 0x05,
      62,   61,   61,   61, 0x05,

 // slots: signature, parameters, type, tag, flags
      97,   61,   61,   61, 0x0a,
     111,   61,   61,   61, 0x0a,

       0        // eod
};

static const char qt_meta_stringdata_letilib__TcpTransportAdapter[] = {
    "letilib::TcpTransportAdapter\0"
    "got_message(QByteArray)\0message\0\0"
    "disconnected(TcpTransportAdapter*)\0"
    "readTcpData()\0disconnecting()\0"
};

void letilib::TcpTransportAdapter::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        Q_ASSERT(staticMetaObject.cast(_o));
        TcpTransportAdapter *_t = static_cast<TcpTransportAdapter *>(_o);
        switch (_id) {
        case 0: _t->got_message((*reinterpret_cast< QByteArray(*)>(_a[1]))); break;
        case 1: _t->disconnected((*reinterpret_cast< TcpTransportAdapter*(*)>(_a[1]))); break;
        case 2: _t->readTcpData(); break;
        case 3: _t->disconnecting(); break;
        default: ;
        }
    }
}

const QMetaObjectExtraData letilib::TcpTransportAdapter::staticMetaObjectExtraData = {
    0,  qt_static_metacall 
};

const QMetaObject letilib::TcpTransportAdapter::staticMetaObject = {
    { &QObject::staticMetaObject, qt_meta_stringdata_letilib__TcpTransportAdapter,
      qt_meta_data_letilib__TcpTransportAdapter, &staticMetaObjectExtraData }
};

#ifdef Q_NO_DATA_RELOCATION
const QMetaObject &letilib::TcpTransportAdapter::getStaticMetaObject() { return staticMetaObject; }
#endif //Q_NO_DATA_RELOCATION

const QMetaObject *letilib::TcpTransportAdapter::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->metaObject : &staticMetaObject;
}

void *letilib::TcpTransportAdapter::qt_metacast(const char *_clname)
{
    if (!_clname) return 0;
    if (!strcmp(_clname, qt_meta_stringdata_letilib__TcpTransportAdapter))
        return static_cast<void*>(const_cast< TcpTransportAdapter*>(this));
    if (!strcmp(_clname, "ITransportable"))
        return static_cast< ITransportable*>(const_cast< TcpTransportAdapter*>(this));
    return QObject::qt_metacast(_clname);
}

int letilib::TcpTransportAdapter::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QObject::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 4)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 4;
    }
    return _id;
}

// SIGNAL 0
void letilib::TcpTransportAdapter::got_message(QByteArray _t1)
{
    void *_a[] = { 0, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 0, _a);
}

// SIGNAL 1
void letilib::TcpTransportAdapter::disconnected(TcpTransportAdapter * _t1)
{
    void *_a[] = { 0, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 1, _a);
}
QT_END_MOC_NAMESPACE
