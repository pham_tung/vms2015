#ifndef XORCACULATOR
#define XORCACULATOR
#include <QByteArray>
QByteArray calculateXor(const QByteArray& data)
{

    QByteArray result;
    for(int i = 0 ; i < data.length(); ++i)
    {
        result.append(data.at(i) ^ 0xAB);
    }
    return result;
}
#endif // XORCACULATOR

