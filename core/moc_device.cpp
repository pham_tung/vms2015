/****************************************************************************
** Meta object code from reading C++ file 'device.h'
**
** Created: Sat Jun 6 17:55:43 2015
**      by: The Qt Meta Object Compiler version 63 (Qt 4.8.2)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "devices/leti/device.h"
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'device.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 63
#error "This file was generated using the moc from 4.8.2. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
static const uint qt_meta_data_letilib__LetiDevice[] = {

 // content:
       6,       // revision
       0,       // classname
       0,    0, // classinfo
       2,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

 // slots: signature, parameters, type, tag, flags
      20,   48,   56,   56, 0x0a,
      57,   92,   56,   56, 0x0a,

       0        // eod
};

static const char qt_meta_stringdata_letilib__LetiDevice[] = {
    "letilib::LetiDevice\0process_message(QByteArray)\0"
    "message\0\0disconnected(TcpTransportAdapter*)\0"
    "adapter\0"
};

void letilib::LetiDevice::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        Q_ASSERT(staticMetaObject.cast(_o));
        LetiDevice *_t = static_cast<LetiDevice *>(_o);
        switch (_id) {
        case 0: _t->process_message((*reinterpret_cast< QByteArray(*)>(_a[1]))); break;
        case 1: _t->disconnected((*reinterpret_cast< TcpTransportAdapter*(*)>(_a[1]))); break;
        default: ;
        }
    }
}

const QMetaObjectExtraData letilib::LetiDevice::staticMetaObjectExtraData = {
    0,  qt_static_metacall 
};

const QMetaObject letilib::LetiDevice::staticMetaObject = {
    { &VMSBaseDevice::staticMetaObject, qt_meta_stringdata_letilib__LetiDevice,
      qt_meta_data_letilib__LetiDevice, &staticMetaObjectExtraData }
};

#ifdef Q_NO_DATA_RELOCATION
const QMetaObject &letilib::LetiDevice::getStaticMetaObject() { return staticMetaObject; }
#endif //Q_NO_DATA_RELOCATION

const QMetaObject *letilib::LetiDevice::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->metaObject : &staticMetaObject;
}

void *letilib::LetiDevice::qt_metacast(const char *_clname)
{
    if (!_clname) return 0;
    if (!strcmp(_clname, qt_meta_stringdata_letilib__LetiDevice))
        return static_cast<void*>(const_cast< LetiDevice*>(this));
    return VMSBaseDevice::qt_metacast(_clname);
}

int letilib::LetiDevice::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = VMSBaseDevice::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 2)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 2;
    }
    return _id;
}
QT_END_MOC_NAMESPACE
