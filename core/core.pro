#-------------------------------------------------
#
# Project created by QtCreator 2015-04-09T10:47:19
#
#-------------------------------------------------

QT       -= gui
QT      += network
TARGET = core
TEMPLATE = lib

DEFINES += CORE_LIBRARY

SOURCES += core.cpp \
    devices/general/vmsdevice.cpp \
    devices/leti/device.cpp \
    transport/tcp/tcptransportadapter.cpp \
    hal/gpio/gpio.cpp \
    hal/i2c/i2c.cpp \
    hal/spi/spi.cpp \
    test.cpp \
    devices/leti/commands.cpp

HEADERS += core.h\
        core_global.h \
    devices/general/vmsdevice.h \
    devices/general/icontrollable.h \
    devices/leti/commands.h \
    utils/singleton.h \
    devices/leti/device.h \
    transport/transportbase.h \
    transport/tcp/tcptransportadapter.h \
    devices/leti/config.h \
    hal/abstracthardware.h \
    hal/gpio/gpio.h \
    hal/i2c/i2c.h \
    hal/spi/spi.h \
    test.h \
    utils/sleeper.h \
    utils/xorcaculator.h

unix {
    target.path = /usr/lib
    INSTALLS += target
}


