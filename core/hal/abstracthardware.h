#ifndef ABSTRACTHARDWARE
#define ABSTRACTHARDWARE

namespace letilib {
class AbstractHardware
{
public:
    int init() = 0;
    int read() = 0;
    int write() = 0;
    int close() = 0;
};
}

#endif // ABSTRACTHARDWARE

