#ifndef SPI_H
#define SPI_H

#include <unistd.h>
#include <stdint.h>
#include <fcntl.h>
#include <sys/ioctl.h>
#ifdef SPIDEV_H
#include <linux/spi/spidev.h>
#else
#define SPI_MODE_0              (0|0)
#endif
#include <stdio.h>
#include <errno.h>
#include <stdlib.h>
#include <string>
#include <iostream>

class SPIDevice{

public:
    SPIDevice();
    SPIDevice(std::string devspi, unsigned char spiMode, unsigned int spiSpeed, unsigned char spibitsPerWord);
    ~SPIDevice();
    int spiWriteRead( unsigned char *data, int length);

private:
    unsigned char mode;
    unsigned char bitsPerWord;
    unsigned int speed;
    int spifd;

    int spiOpen(std::string devspi);
    int spiClose();

};

#endif // SPI_H
