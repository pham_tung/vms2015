#ifndef I2C_H
#define I2C_H

#include <string>
using namespace std;

class I2CDevice{

public:
    I2CDevice();
    I2CDevice(string devi2c);
    ~I2CDevice();

private:
    unsigned char mode;
    unsigned char bitsPerWord;
    unsigned int speed;
    int spifd;

    int spiOpen(std::string devspi);
    int spiClose();

};

#endif // I2C_H
