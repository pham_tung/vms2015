#ifndef VMSDEVICE_H
#define VMSDEVICE_H

#include <QObject>
#include "icontrollable.h"
#include "core_global.h"
namespace letilib

{
class CORESHARED_EXPORT VMSBaseDevice : public QObject,public IControllable
{
    Q_OBJECT

public:

    explicit VMSBaseDevice(QObject* parent);
    VMSBaseDevice(QString id = "", QString imei = "");
    VMSBaseDevice(const QString& filepath);
    ~VMSBaseDevice();
    Q_PROPERTY(QString id READ getID WRITE setID)
    Q_PROPERTY(QString imei READ getIMEI WRITE setIMEI)

signals:

public slots:

    // IControllable interface
public:
    QString getID() const;
    void setID(const QString& id);

    QString getIMEI() const;
    void setIMEI(const QString & imei);

    void save(QString path);
private:
    QString m_id;
    QString m_imei;
};

}
#endif // VMSDEVICE_H
