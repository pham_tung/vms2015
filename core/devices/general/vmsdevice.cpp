#include "vmsdevice.h"
//#include "../../../../../qjson/src/parser.h"
//#include "../../../../../qjson/src/serializer.h"
using namespace letilib;

VMSBaseDevice::VMSBaseDevice(QObject* parent)
{

}

VMSBaseDevice::~VMSBaseDevice()
{

}

VMSBaseDevice::VMSBaseDevice(QString id, QString imei)
{
    setID(id);
    setIMEI(imei);
}

VMSBaseDevice::VMSBaseDevice(const QString& jsonData)
{
    /*
    QJson::Parser parser;
    QVariant variant = parser.parse(jsonData);

    QObjectHelper::qvariant2qobject(variant.toMap(), &this);
    */
}

void VMSBaseDevice::setID(const QString &id)
{
    m_id = id;
}

QString VMSBaseDevice::getID() const
{
    return m_id;
}

void VMSBaseDevice::setIMEI(const QString &imei)
{
    m_imei = imei;
}

QString VMSBaseDevice::getIMEI() const
{
    return m_imei;
}

void VMSBaseDevice::save(QString path)
{
    /*
    QVariantMap variant = QObjectHelper::qobject2qvariant(&person);
    QJson::Serializer serializer;
    qDebug() << serializer.serialize( variant);
    */
}
