// IControllable.h
#ifndef ICONTROLLABLE
#define ICONTROLLABLE
namespace letilib {

class IControllable
{

public:
    virtual QString getID() const = 0;
    virtual QString getIMEI() const = 0;
    //virtual QString getName() const = 0;
};

}
#endif // ICONTROLLABLE

