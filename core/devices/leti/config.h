#ifndef CONFIG
#define CONFIG
#define DUMMY_CLIENT


#ifndef SERVER_IP
#define SERVER_IP "10.15.154.25"
#endif

#ifndef SERVER_PORT_CONTROL
#define SERVER_PORT_CONTROL 6868
#endif

#ifndef SERVER_PORT_REPORT
#define SERVER_PORT_REPORT 8686
#endif

#ifdef DUMMY_CLIENT
#define DUMMY_IMEI "111111111111110"
#define DUMMY_ID "dummy"
#define APP_VERSION "dummy_1.0"
#endif
#endif // CONFIG

