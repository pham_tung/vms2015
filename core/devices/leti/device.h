#ifndef LETIDEVICE_H
#define LETIDEVICE_H
#include "transport/tcp/tcptransportadapter.h"
#include "devices/general/vmsdevice.h"
#include <QObject>
#include "config.h"
#include "commands.h"
#include "errno.h"
namespace letilib {
    class LetiDevice : public VMSBaseDevice
    {
        Q_OBJECT
    private:
        TcpTransportAdapter* control_adapter;
        TcpTransportAdapter* report_adapter;
        int m_retry_count;
    public:
        explicit LetiDevice(QObject* parent);
        LetiDevice(QString id = DUMMY_ID, QString imei = DUMMY_IMEI);
        ~LetiDevice();
        error_t login();

        QByteArray getTime();
        void setTime(QByteArray data);
    public slots:
        void process_message(QByteArray message);
        void disconnected(TcpTransportAdapter *adapter);
    protected:

        error_t sendData(QByteArray *buffer);
        error_t sendBuffer2Control(QByteArray *buffer);
        error_t sendBuffer2Report(QByteArray *buffer);
    };
}
#endif // LETIDEVICE_H
