#ifndef COMMANDS
#define COMMANDS
#include "config.h"

#include <QObject>
#include <QDataStream>
#include <QMetaEnum>

#define ENUM_NAME(o,e,v) (o::staticMetaObject.enumerator(o::staticMetaObject.indexOfEnumerator(#e)).valueToKey(v))
namespace letilib {


class LetiData : public QObject
{
    Q_OBJECT
    Q_ENUMS(TAG COMMAND WARNING)

    Q_PROPERTY(TAG tag READ Tag WRITE setTag)
    Q_PROPERTY(COMMAND command READ Command WRITE setCommand)
    Q_PROPERTY(QByteArray data READ Data WRITE setData)


public:

    //LetiData &operator<<(const LetiData &);
    //LetiData &operator>>(const char *str);

    static const int FONT_PACKAGE_LENGTH = 1024;
    static const int DATA_PACKAGE_LENGTH = 1024;
    static const int FIRMWARE_PACKAGE_LENGTH = 1024;

    enum TAG {
        SERVER = 0x00,
        DEVICE,
        PING = 0x03
    };

    enum COMMAND
    {
        // Font data update commands
        CMD_SERVER_FONT_UPDATE_REQ = 0x20,
        FONT_INFO_HEADER,
        FONT_PACKAGE_HEADER,

        // Bitmap data update commands
        CMD_SERVER_DATA_UPDATE_REQ = 0x30,
        DATA_INFO_HEADER,
        DATA_PACKAGE_HEADER,

        // FIRMWARE update commands
        CMD_SERVER_FIRMWARE_UPDATE_REQ = 0x40,
        FIRMWARE_INFO_HEADER,
        FIRMWARE_PACKAGE_HEADER,

        // Image binary commands
        CMD_INSERT_IMAGE = 0x50,
        CMD_UPDATE_IMAGE,
        CMD_DELETE_IMAGE,
        CMD_DELETE_ALL_IMAGE,
        CMD_GET_LIST_IMAGE_INDEX,

        // Page control commands
        CMD_CREATE_PAGE = 0x60,
        CMD_UPDATE_PAGE,
        CMD_DELETE_PAGE,
        CMD_GET_PAGE_INFO,
        CMD_SET_ACTIVE_PAGE,
        CMD_GET_ACTIVE_PAGE,
        CMD_GET_LIST_PAGE_INDEX,

        // Popup control commands
        CMD_INSERT_POPUP = 0x70,
        CMD_UPDATE_POPUP,
        CMD_DELETE_POPUP,

        // Playlist control commands
        CMD_SERVER_PLAYLIST_UPDATE_REQ = 0x80,
        PLAYLIST_INFO_HEADER,
        CMD_INSERT_IMAGE_TO_PLAYLIST,
        CMD_UPDATE_IMAGE_TO_PLAYLIST,
        CMD_DELETE_IMAGE_FROM_PLAYLIST,

        // Control commands server sent to device
        GET_VERSION = 0xA0,
        GET_PASSWORD,
        SET_PASSWORD,
        GET_BRIGHTNESS,
        SET_BRIGHTNESS,
        // CMD_UPDATE_BUS_DATA,// Temporary disable, just hardcode for testing
        CMD_CHECK_LED,
        CMD_CLEAR_LED,
        CMD_START_SCAN_LED,
        CMD_STOP_SCAN_LED,
        CMD_CAPTURE_SCREEN,
        CMD_GET_STATUS,
        CMD_REBOOT,
        CMD_SET_PREDEFINE,
        CMD_SHUTDOWN,
        CMD_SET_TIME,
        CMD_GET_TIME,

        // Device commands sent back to server
        CMD_SEND_IMEI = 0x5D,
        // Screen data sent from device
        LED_INFO_HEADER,
        LED_SCREEN_DATA,
        LED_STATUS_DATA
    };


    enum WARNING{
        TOO_HOT,
        DAMAGED,
        OUT_OF_POWER,
        OPENNED,
    } ;

    TAG Tag() const
    {
        return m_Tag;
    }
    COMMAND Command() const
    {
        return m_Command;
    }

    QByteArray Data() const
    {
        return m_Data;
    }

    int DataLength() const
    {
        return m_DataLength;
    }
public slots:
    void setTag(TAG arg)
    {
        m_Tag = arg;
    }
    void setCommand(COMMAND arg)
    {
        m_Command = arg;
    }
    void setData(QByteArray arg)
    {
        m_Data = arg;
    }
    void setDataLength(int arg)
    {
        m_DataLength = arg;
    }

private:

    TAG m_Tag;

    COMMAND m_Command;

    QByteArray m_Data;
    int m_DataLength;
};

QDataStream &operator <<(QDataStream &out, const LetiData &obj);
QDataStream &operator >>(QDataStream &in, LetiData &obj);



}

/*
typedef std::vector<char> buffer_t;
#pragma pack(1)
typedef struct
{
    char type;
    char command;
    char* buffer;

} command_t;

//typedef union {
//    struct data{
//        command_header_t header;
//        char* buffer;
//    };
//    char data_arr[sizeof(struct data)];
//}command_t;


command_t warning_temp ={DEVICE_TAG,WARNING_TOO_HOT};
command_t warning_volt ={DEVICE_TAG,WARNING_OUT_OF_POWER};
command_t warning_open ={DEVICE_TAG,WARNING_OPENNED};
}
*/
#endif // COMMANDS

