#include "device.h"
#include "utils/sleeper.h"
#include <QByteArray>
using namespace letilib;

LetiDevice::LetiDevice(QObject* parent):m_retry_count(0)
{

}

LetiDevice::LetiDevice(QString id , QString imei)
    :VMSBaseDevice(id,imei),m_retry_count(0)
{
    control_adapter = new TcpTransportAdapter(SERVER_IP,SERVER_PORT_CONTROL );
    connect(control_adapter,SIGNAL(got_message(QByteArray)),this,SLOT(process_message(QByteArray)));
    connect( control_adapter, SIGNAL(disconnected(TcpTransportAdapter*)),this,SLOT(disconnected(TcpTransportAdapter*)));
    report_adapter = new TcpTransportAdapter(SERVER_IP,SERVER_PORT_REPORT );
    // open connection
    control_adapter->open();

    Sleeper::msleep(1000);

    // open connection
    report_adapter->open();
}

LetiDevice::~LetiDevice()
{
    if (control_adapter)
        delete control_adapter;
    if (report_adapter)
        delete report_adapter;
}

void LetiDevice::disconnected(TcpTransportAdapter* adapter)
{
    do
    {
        qDebug() << "socket disconnected! Try to reconnect " << ++m_retry_count;
        sleep(1);
    }while (!adapter->open());
    QByteArray buffer;
    buffer.append(LetiData::DEVICE);
    buffer.append(LetiData::CMD_SEND_IMEI);
    buffer.append(this->getIMEI());
    adapter->write(buffer.data());

}
error_t LetiDevice::login()
{
    QByteArray buffer;
    buffer.append(LetiData::DEVICE);
    buffer.append(LetiData::CMD_SEND_IMEI);
    buffer.append(this->getIMEI());

    sendBuffer2Control(&buffer);
    sendBuffer2Report(&buffer);

}

void LetiDevice::process_message(QByteArray message)
{
    qDebug() << message.toHex();
    LetiData data;
    data.setDataLength(message.length());
    QDataStream stream (&message,QIODevice::ReadWrite);
    stream >> data;
    //stream << data;
    //QByteArray buff;
    //stream.readRawData(buff.data(),message.length()+6);
    //qDebug() << buff;
    qDebug() << "parse data: "<< data.Tag()
             << data.Command()
             << data.Data().toHex();
    if (data.Tag() == LetiData::PING)
    {
        qDebug() << "got ping message";
    }
    else if (data.Tag() == LetiData::SERVER)
    {
        switch(data.Command())
        {
        case LetiData::CMD_SET_TIME:
            setTime(data.Data());
            break;
        case LetiData::GET_VERSION :
            break;
        case LetiData::GET_PASSWORD:
        case LetiData::SET_PASSWORD:
        case LetiData::GET_BRIGHTNESS:
        case LetiData::SET_BRIGHTNESS:
        case LetiData::CMD_CHECK_LED:
        case LetiData::CMD_CLEAR_LED:
        case LetiData::CMD_START_SCAN_LED:
        case LetiData::CMD_STOP_SCAN_LED:
        case LetiData::CMD_CAPTURE_SCREEN:
        case LetiData::CMD_GET_STATUS:
        case LetiData::CMD_REBOOT:
        case LetiData::CMD_SET_PREDEFINE:
        case LetiData::CMD_SHUTDOWN:
        case LetiData::CMD_GET_TIME:
            break;
        }
    }
}

QByteArray LetiDevice::getTime()
{
    QByteArray data;
    data[0] = 0x00;
    data.append(0x01);
    data.append(0x02);
    data.append(0x03);
    data.append(0x04);
    data.append(0x05);
    return data;
}

void LetiDevice::setTime(QByteArray data)
{
    qDebug() << data[0];
    qDebug() << data[1];
    qDebug() << data[2];
    qDebug() << data[3];
    qDebug() << data[4];
    qDebug() << data[5];
    //qDebug() << data[6];
}

error_t LetiDevice::sendData(QByteArray *buffer)
{

    sendBuffer2Control(buffer);
    sendBuffer2Report(buffer);

}

error_t LetiDevice::sendBuffer2Control(QByteArray *buffer)
{
    if (control_adapter && control_adapter->ready())
    {

        control_adapter->write(buffer->data());
    }
}

error_t LetiDevice::sendBuffer2Report(QByteArray *buffer)
{
    if (report_adapter && report_adapter->ready())
    {
        report_adapter->write(buffer->data());

    }
}
