
#include "commands.h"
#include <QDebug>
namespace letilib {


QDataStream &operator <<(QDataStream &out, const LetiData &obj)
{
    qDebug() << "out";
    out << "output "<< ENUM_NAME(LetiData,LetiData::TAG ,obj.Tag())
        << ENUM_NAME(LetiData, LetiData::COMMAND,obj.Command())
        << obj.Data().toHex();
    return out;
}

QDataStream &operator >>(QDataStream &in, LetiData &obj)
{
    qDebug() << "tag";
    quint8 tag;
    in >> tag ;
    obj.setTag(static_cast<LetiData::TAG>(tag));
    qDebug() << "com";
    quint8 command;
    in >> command;
    obj.setCommand(static_cast<LetiData::COMMAND>(command));
    qDebug() << "len" << obj.DataLength();
    QByteArray buff;
    //in >> data;
    in.readRawData(buff.data(),obj.DataLength());
    obj.setData(buff);
    return in;
}
}
