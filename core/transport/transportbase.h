#ifndef TRANSPORTBASE_H
#define TRANSPORTBASE_H

class ITransportable
{
public:
    virtual int open() = 0;
    virtual int read(char* data, int maxlength) = 0;
    virtual int write(char *buffer) = 0;
    virtual void close() = 0;
};

#endif // TRANSPORTBASE_H
