#ifndef TCPTRANSPORTADAPTER_H
#define TCPTRANSPORTADAPTER_H
#include <QtNetwork/QTcpSocket>
#include <QByteArray>
#include <QObject>
#include "transport/transportbase.h"
namespace letilib {


class TcpTransportAdapter :public QObject,/* public QThread,*/ public ITransportable
{
    Q_OBJECT
private:
    QTcpSocket* socket;
    QString m_ip;
    uint m_port;
    bool m_ready;

public:
    explicit TcpTransportAdapter();
    TcpTransportAdapter(QString ip = "",uint port = 0);
    ~TcpTransportAdapter();
    void run();
signals:
    void got_message(QByteArray message);
    void disconnected(TcpTransportAdapter*);
private:
    void processMessage(QByteArray message);
    QByteArray data;
    // ITransportable interface
public:
    int open();
    int read(char* data, int maxlength);
    int write(char *buffer);
    void close();
    bool ready();

public slots:
    void readTcpData();
    void disconnecting();
};
}
#endif // TCPTRANSPORTADAPTER_H
