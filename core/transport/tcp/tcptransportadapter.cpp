#include "tcptransportadapter.h"
#include "core_global.h"
#include "devices/leti/commands.h"
#include "utils/xorcaculator.h"
using namespace letilib;

TcpTransportAdapter::TcpTransportAdapter()
    :m_ip(""),m_port(0),m_ready(false) {}

TcpTransportAdapter::TcpTransportAdapter(QString ip,uint port)
    :m_ip(ip),m_port(port),m_ready(false){}

TcpTransportAdapter::~TcpTransportAdapter()
{
    qDebug() << "des";
    if (!socket && socket->isOpen())
    {
        socket->close();
        delete socket;
    }
//    if (this->isRunning())
//        this->terminate();
}

void TcpTransportAdapter::run()
{

    open();
    //while(1){}
    //exec();
}


int TcpTransportAdapter::open()
{
    if (m_ip.isNull() || !m_port)
        return error_common;

    if (socket && socket->isOpen())
    {
        socket->abort();
        disconnect( socket, SIGNAL(readyRead()),this, SLOT(readTcpData()) );

        disconnect( socket, SIGNAL(disconnected()),this,SLOT(disconnecting()));
        delete socket;
    }

    socket = new QTcpSocket();

    connect( socket, SIGNAL(readyRead()),this, SLOT(readTcpData()),Qt::DirectConnection );

    connect( socket, SIGNAL(disconnected()),this,SLOT(disconnecting()));

    socket->connectToHost(m_ip, m_port);
    if(socket->waitForConnected(5000)) {
        qDebug() << "Adapter started at: " << m_ip << ":" << m_port;
        m_ready = true;

        return success;
    }
    qDebug() << "Error: " << socket->errorString();
    m_ready = false;
    return error_socket;
}

void TcpTransportAdapter::disconnecting()
{
    emit disconnected(this);
}

void TcpTransportAdapter::readTcpData()
{
    //qDebug() << "tcp read data";
    data = socket->readAll();
    /*
        while(!data.contains('\n'))
        {
            socket->waitForReadyRead();
            data += socket->readAll();
        }
        int bytes = data.indexOf('\n')+1;
        QByteArray message = data.left(bytes);
        data = data.mid(bytes);
        */
        //qDebug() <<data.length() << " : " << data.toHex();
        processMessage(data);
}

void TcpTransportAdapter::processMessage(QByteArray message)
{
    QByteArray decoded_data = calculateXor(message);
    //qDebug() <<message.toHex() << " : " << decoded_data.toHex() << " decode processed";
    emit got_message(decoded_data);
}

int TcpTransportAdapter::read(char* data, int maxlength)
{
    if (socket && socket->isOpen() )
    {
        if (! socket->isReadable())
            socket->waitForReadyRead();

        return socket->read(data,maxlength);
    }
}

int TcpTransportAdapter::write(char *buffer)
{
    if (socket && socket->isOpen() && socket->isWritable())
    {
        quint64 ret = socket->write(buffer);
        socket->waitForBytesWritten();
        return ret;
    }
}

void TcpTransportAdapter::close()
{
    if (!socket && socket->isOpen())
    {
        socket->close();
        delete socket;
    }
}

bool TcpTransportAdapter::ready()
{
    return m_ready;
}
