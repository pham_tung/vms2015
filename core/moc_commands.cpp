/****************************************************************************
** Meta object code from reading C++ file 'commands.h'
**
** Created: Sat Jun 6 17:55:40 2015
**      by: The Qt Meta Object Compiler version 63 (Qt 4.8.2)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "devices/leti/commands.h"
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'commands.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 63
#error "This file was generated using the moc from 4.8.2. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
static const uint qt_meta_data_letilib__LetiData[] = {

 // content:
       6,       // revision
       0,       // classname
       0,    0, // classinfo
       4,   14, // methods
       3,   34, // properties
       3,   43, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

 // slots: signature, parameters, type, tag, flags
      18,   30,   34,   34, 0x0a,
      35,   30,   34,   34, 0x0a,
      55,   30,   34,   34, 0x0a,
      75,   30,   34,   34, 0x0a,

 // properties: name, type, flags
      94,   98, 0x0009510b,
     102,  110, 0x0009510b,
     118,  123, 0x0c095103,

 // enums: name, flags, count, data
      98, 0x0,    3,   55,
     110, 0x0,   49,   61,
     134, 0x0,    4,  159,

 // enum data: key, value
     142, uint(letilib::LetiData::SERVER),
     149, uint(letilib::LetiData::DEVICE),
     156, uint(letilib::LetiData::PING),
     161, uint(letilib::LetiData::CMD_SERVER_FONT_UPDATE_REQ),
     188, uint(letilib::LetiData::FONT_INFO_HEADER),
     205, uint(letilib::LetiData::FONT_PACKAGE_HEADER),
     225, uint(letilib::LetiData::CMD_SERVER_DATA_UPDATE_REQ),
     252, uint(letilib::LetiData::DATA_INFO_HEADER),
     269, uint(letilib::LetiData::DATA_PACKAGE_HEADER),
     289, uint(letilib::LetiData::CMD_SERVER_FIRMWARE_UPDATE_REQ),
     320, uint(letilib::LetiData::FIRMWARE_INFO_HEADER),
     341, uint(letilib::LetiData::FIRMWARE_PACKAGE_HEADER),
     365, uint(letilib::LetiData::CMD_INSERT_IMAGE),
     382, uint(letilib::LetiData::CMD_UPDATE_IMAGE),
     399, uint(letilib::LetiData::CMD_DELETE_IMAGE),
     416, uint(letilib::LetiData::CMD_DELETE_ALL_IMAGE),
     437, uint(letilib::LetiData::CMD_GET_LIST_IMAGE_INDEX),
     462, uint(letilib::LetiData::CMD_CREATE_PAGE),
     478, uint(letilib::LetiData::CMD_UPDATE_PAGE),
     494, uint(letilib::LetiData::CMD_DELETE_PAGE),
     510, uint(letilib::LetiData::CMD_GET_PAGE_INFO),
     528, uint(letilib::LetiData::CMD_SET_ACTIVE_PAGE),
     548, uint(letilib::LetiData::CMD_GET_ACTIVE_PAGE),
     568, uint(letilib::LetiData::CMD_GET_LIST_PAGE_INDEX),
     592, uint(letilib::LetiData::CMD_INSERT_POPUP),
     609, uint(letilib::LetiData::CMD_UPDATE_POPUP),
     626, uint(letilib::LetiData::CMD_DELETE_POPUP),
     643, uint(letilib::LetiData::CMD_SERVER_PLAYLIST_UPDATE_REQ),
     674, uint(letilib::LetiData::PLAYLIST_INFO_HEADER),
     695, uint(letilib::LetiData::CMD_INSERT_IMAGE_TO_PLAYLIST),
     724, uint(letilib::LetiData::CMD_UPDATE_IMAGE_TO_PLAYLIST),
     753, uint(letilib::LetiData::CMD_DELETE_IMAGE_FROM_PLAYLIST),
     784, uint(letilib::LetiData::GET_VERSION),
     796, uint(letilib::LetiData::GET_PASSWORD),
     809, uint(letilib::LetiData::SET_PASSWORD),
     822, uint(letilib::LetiData::GET_BRIGHTNESS),
     837, uint(letilib::LetiData::SET_BRIGHTNESS),
     852, uint(letilib::LetiData::CMD_CHECK_LED),
     866, uint(letilib::LetiData::CMD_CLEAR_LED),
     880, uint(letilib::LetiData::CMD_START_SCAN_LED),
     899, uint(letilib::LetiData::CMD_STOP_SCAN_LED),
     917, uint(letilib::LetiData::CMD_CAPTURE_SCREEN),
     936, uint(letilib::LetiData::CMD_GET_STATUS),
     951, uint(letilib::LetiData::CMD_REBOOT),
     962, uint(letilib::LetiData::CMD_SET_PREDEFINE),
     980, uint(letilib::LetiData::CMD_SHUTDOWN),
     993, uint(letilib::LetiData::CMD_SET_TIME),
    1006, uint(letilib::LetiData::CMD_GET_TIME),
    1019, uint(letilib::LetiData::CMD_SEND_IMEI),
    1033, uint(letilib::LetiData::LED_INFO_HEADER),
    1049, uint(letilib::LetiData::LED_SCREEN_DATA),
    1065, uint(letilib::LetiData::LED_STATUS_DATA),
    1081, uint(letilib::LetiData::TOO_HOT),
    1089, uint(letilib::LetiData::DAMAGED),
    1097, uint(letilib::LetiData::OUT_OF_POWER),
    1110, uint(letilib::LetiData::OPENNED),

       0        // eod
};

static const char qt_meta_stringdata_letilib__LetiData[] = {
    "letilib::LetiData\0setTag(TAG)\0arg\0\0"
    "setCommand(COMMAND)\0setData(QByteArray)\0"
    "setDataLength(int)\0tag\0TAG\0command\0"
    "COMMAND\0data\0QByteArray\0WARNING\0SERVER\0"
    "DEVICE\0PING\0CMD_SERVER_FONT_UPDATE_REQ\0"
    "FONT_INFO_HEADER\0FONT_PACKAGE_HEADER\0"
    "CMD_SERVER_DATA_UPDATE_REQ\0DATA_INFO_HEADER\0"
    "DATA_PACKAGE_HEADER\0CMD_SERVER_FIRMWARE_UPDATE_REQ\0"
    "FIRMWARE_INFO_HEADER\0FIRMWARE_PACKAGE_HEADER\0"
    "CMD_INSERT_IMAGE\0CMD_UPDATE_IMAGE\0"
    "CMD_DELETE_IMAGE\0CMD_DELETE_ALL_IMAGE\0"
    "CMD_GET_LIST_IMAGE_INDEX\0CMD_CREATE_PAGE\0"
    "CMD_UPDATE_PAGE\0CMD_DELETE_PAGE\0"
    "CMD_GET_PAGE_INFO\0CMD_SET_ACTIVE_PAGE\0"
    "CMD_GET_ACTIVE_PAGE\0CMD_GET_LIST_PAGE_INDEX\0"
    "CMD_INSERT_POPUP\0CMD_UPDATE_POPUP\0"
    "CMD_DELETE_POPUP\0CMD_SERVER_PLAYLIST_UPDATE_REQ\0"
    "PLAYLIST_INFO_HEADER\0CMD_INSERT_IMAGE_TO_PLAYLIST\0"
    "CMD_UPDATE_IMAGE_TO_PLAYLIST\0"
    "CMD_DELETE_IMAGE_FROM_PLAYLIST\0"
    "GET_VERSION\0GET_PASSWORD\0SET_PASSWORD\0"
    "GET_BRIGHTNESS\0SET_BRIGHTNESS\0"
    "CMD_CHECK_LED\0CMD_CLEAR_LED\0"
    "CMD_START_SCAN_LED\0CMD_STOP_SCAN_LED\0"
    "CMD_CAPTURE_SCREEN\0CMD_GET_STATUS\0"
    "CMD_REBOOT\0CMD_SET_PREDEFINE\0CMD_SHUTDOWN\0"
    "CMD_SET_TIME\0CMD_GET_TIME\0CMD_SEND_IMEI\0"
    "LED_INFO_HEADER\0LED_SCREEN_DATA\0"
    "LED_STATUS_DATA\0TOO_HOT\0DAMAGED\0"
    "OUT_OF_POWER\0OPENNED\0"
};

void letilib::LetiData::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        Q_ASSERT(staticMetaObject.cast(_o));
        LetiData *_t = static_cast<LetiData *>(_o);
        switch (_id) {
        case 0: _t->setTag((*reinterpret_cast< TAG(*)>(_a[1]))); break;
        case 1: _t->setCommand((*reinterpret_cast< COMMAND(*)>(_a[1]))); break;
        case 2: _t->setData((*reinterpret_cast< QByteArray(*)>(_a[1]))); break;
        case 3: _t->setDataLength((*reinterpret_cast< int(*)>(_a[1]))); break;
        default: ;
        }
    }
}

const QMetaObjectExtraData letilib::LetiData::staticMetaObjectExtraData = {
    0,  qt_static_metacall 
};

const QMetaObject letilib::LetiData::staticMetaObject = {
    { &QObject::staticMetaObject, qt_meta_stringdata_letilib__LetiData,
      qt_meta_data_letilib__LetiData, &staticMetaObjectExtraData }
};

#ifdef Q_NO_DATA_RELOCATION
const QMetaObject &letilib::LetiData::getStaticMetaObject() { return staticMetaObject; }
#endif //Q_NO_DATA_RELOCATION

const QMetaObject *letilib::LetiData::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->metaObject : &staticMetaObject;
}

void *letilib::LetiData::qt_metacast(const char *_clname)
{
    if (!_clname) return 0;
    if (!strcmp(_clname, qt_meta_stringdata_letilib__LetiData))
        return static_cast<void*>(const_cast< LetiData*>(this));
    return QObject::qt_metacast(_clname);
}

int letilib::LetiData::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QObject::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 4)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 4;
    }
#ifndef QT_NO_PROPERTIES
      else if (_c == QMetaObject::ReadProperty) {
        void *_v = _a[0];
        switch (_id) {
        case 0: *reinterpret_cast< TAG*>(_v) = Tag(); break;
        case 1: *reinterpret_cast< COMMAND*>(_v) = Command(); break;
        case 2: *reinterpret_cast< QByteArray*>(_v) = Data(); break;
        }
        _id -= 3;
    } else if (_c == QMetaObject::WriteProperty) {
        void *_v = _a[0];
        switch (_id) {
        case 0: setTag(*reinterpret_cast< TAG*>(_v)); break;
        case 1: setCommand(*reinterpret_cast< COMMAND*>(_v)); break;
        case 2: setData(*reinterpret_cast< QByteArray*>(_v)); break;
        }
        _id -= 3;
    } else if (_c == QMetaObject::ResetProperty) {
        _id -= 3;
    } else if (_c == QMetaObject::QueryPropertyDesignable) {
        _id -= 3;
    } else if (_c == QMetaObject::QueryPropertyScriptable) {
        _id -= 3;
    } else if (_c == QMetaObject::QueryPropertyStored) {
        _id -= 3;
    } else if (_c == QMetaObject::QueryPropertyEditable) {
        _id -= 3;
    } else if (_c == QMetaObject::QueryPropertyUser) {
        _id -= 3;
    }
#endif // QT_NO_PROPERTIES
    return _id;
}
QT_END_MOC_NAMESPACE
