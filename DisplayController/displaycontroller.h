#ifndef DISPLAYCONTROLLER_H
#define DISPLAYCONTROLLER_H

#include <QObject>
#include "common.h"
#include "dataStruct.h"
#include "displaycontroller_global.h"
#include "QtExtSerialPort/qextserialport.h"
#include "QtExtSerialPort/qextserialenumerator.h"

//class QTimer;
class QextSerialPort;
class QextSerialEnumerator;


class DisplayController: public QextSerialPort
{
    Q_OBJECT

public:
    //QTimer *timer;
    //QextSerialPort *port;
    //QextSerialEnumerator *enumerator;

//private slots:
//    void onReadyRead();
//    void onDsrChanged(bool status);

    // Contructor
public:
    DisplayController();

    // Destructor
    ~DisplayController();

    bool openPort();

    void sendDisplayCmd(DISPLAY_COMMAND cmd, QByteArray cmdParam, u8 paramLength);
    void getVersion();
    void startScan();
    void stopScan();
    void clearScreen();
    void captureScreen();
    void createPopup();
    void updatePopup();
    void clearPopup();
    void deletePopup();
    void changeBrightness();
    void sendPredefine();
    void playPredefine();
    void sendFont();
    void sendPopup(POPUP popup,u8 cmd);
    F_RETURN swapPage(u16 pageID);


};

#endif // DISPLAYCONTROLLER_H
