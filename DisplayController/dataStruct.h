#ifndef DATASTRUCT_H
#define DATASTRUCT_H

#include "common.h"
#include "typedef.h"

#pragma pack(1)
typedef struct
{
    u_int8_t playOrder;
    u_int16_t TTL;
    u_int16_t TTS;
    PLAYLIST_DATA dataType;
    u_int16_t length;
} PLAYLIST_HEADER;
typedef struct
{
    PLAYLIST_HEADER header;
    u_int8_t* data;
    u_int16_t TTL_count;
    bool isLive;
} PLAYLIST_ITEM;
typedef struct
{
    u_int8_t year;
    u_int8_t month;
    u_int8_t day;
    u_int8_t hour;
    u_int8_t minute;
    u_int8_t sec;
} DATETIME;
typedef struct
{
    u_int16_t ID;
    u_int16_t x;
    u_int16_t y;
    u_int8_t z;
    u_int16_t width;
    u_int16_t height;
    u_int8_t dataType;
    u_int8_t itemCount;
    u_int8_t activeItem;
    u_int16_t countDown;
    DATETIME startDate;
    DATETIME endDate;
    bool isUpdate;
} POPUP_HEADER;
typedef union
{
    struct
    {
        POPUP_HEADER header;
        void* data;
    } Info;
    struct
    {
        u_int8_t b[12];
    } byte;
} POPUP;
typedef struct
{
    u_int16_t ID;
    u_int8_t itemCount;
    u_int8_t eventType;
} PAGE_HEADER;
typedef struct
{
    PAGE_HEADER header;
    POPUP *popup;
} PAGE;
typedef struct
{
    BYTE Dummy;
    WORD ledWidth;
    WORD ledHeight;
    BYTE ledType;
    BYTE imei[15];
} HW_CONFIG;
typedef struct
{
    BYTE maxTemp;
} WN_CONFIG;

typedef struct
{
    u_int8_t temp;
    u_int8_t SD_Status;
    u_int32_t SD_Free_Mem;
} SYSTEM_STATUS;
#endif // DATASTRUCT_H
