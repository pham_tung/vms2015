#ifndef __TYPEDEF__H__
#define __TYPEDEF__H__

#include <QObject>

// integer number define for FASFS
/* These types must be 16-bit, 32-bit or larger integer */
typedef int				INT;
typedef unsigned int	UINT;

/* These types must be 8-bit integer */
typedef u_int8_t        u8;
typedef char			CHAR;
typedef unsigned char	UCHAR;
typedef unsigned char	BYTE;

/* These types must be 16-bit integer */
typedef u_int16_t       u16;
typedef short			SHORT;
typedef unsigned short	USHORT;
typedef unsigned short	WORD;
typedef unsigned short	WCHAR;

/* These types must be 32-bit integer */
typedef u_int32_t       u32;
typedef long			LONG;
typedef unsigned long	ULONG;
typedef unsigned long	DWORD;


#endif
