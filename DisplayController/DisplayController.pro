#-------------------------------------------------
#
# Project created by QtCreator 2015-07-07T00:59:03
#
#-------------------------------------------------

QT       -= gui

TARGET = DisplayController
TEMPLATE = lib
CONFIG += staticlib
CONFIG += extserialport

SOURCES += displaycontroller.cpp

HEADERS += displaycontroller.h \
    dataStruct.h \
    common.h \
    typedef.h
unix {
    target.path = /usr/lib
    INSTALLS += target
}
