#include "displaycontroller.h"
#include "QByteArray"
#include "unistd.h"

DisplayController::DisplayController()
{

}

DisplayController::~DisplayController()
{
}

bool DisplayController::openPort()
{
    //! [1]
    this->setPortName("ttyUSB0");
    this->setBaudRate(BAUD115200);
    this->setDataBits(DATA_8);
    this->setParity(PAR_NONE);
    this->setStopBits(STOP_1);
    this->setFlowControl(FLOW_OFF);
    this->setTimeout(10);
    this->setQueryMode(QextSerialPort::EventDriven);
    //this = new QextSerialPort("ttyUSB0", settings, QextSerialPort::EventDriven);
    //! [1]
    //enumerator = new QextSerialEnumerator(this);
    //enumerator->setUpNotifications();

    if (!this->isOpen()) {
        this->open(QIODevice::ReadWrite);
    }

    //If using polling mode, we need a QTimer
//    if (port->isOpen() && port->queryMode() == QextSerialPort::Polling)
//        timer->start();
//    else
//        timer->stop();

    if(this->isOpen())
    {
        return true;
    }
    else
    {
        return false;
    }
}

//void DisplayController::onReadyRead()
//{
//    QByteArray bytes;
//    int a = port->bytesAvailable();
//    bytes.resize(a);
//    port->read(bytes.data(), bytes.size());
//    //qDebug() << "bytes read:" << bytes.size();
//    //qDebug() << "bytes:" << bytes;
//}

//void DisplayController::onDsrChanged(bool status)
//{
//    if (status)
//        qDebug() << "device was turned on";
//    else
//        qDebug() << "device was turned off";
//}


void DisplayController::sendDisplayCmd(DISPLAY_COMMAND cmd, QByteArray cmdParam,u8 paramLength)
{
    int i = 0;
    if (this->isOpen())
    {
        this->putChar(MAIN_TAG);
        this->putChar(cmd);
        if(i>0)
        {
            for(i = 0; i< paramLength;i++)
            {
                this->putChar(cmdParam[i]);
            }
        }
        this->putChar('\r');
        this->putChar('\n');
    }
}

void DisplayController::getVersion()
{
    sendDisplayCmd(DPL_CMD_FW_INFO,NULL,0);
}

void DisplayController::startScan()
{
    sendDisplayCmd(DPL_CMD_START_SCAN_LED,NULL,0);
}

void DisplayController::stopScan()
{
    sendDisplayCmd(DPL_CMD_STOP_SCAN_LED,NULL,0);
}

void DisplayController::clearScreen()
{
    sendDisplayCmd(DPL_CMD_CLEAR_SCREEN,NULL,0);
}

void DisplayController::captureScreen()
{
    sendDisplayCmd(DPL_CMD_CAPTURE_SCREEN,NULL,0);
}

void DisplayController::updatePopup()
{

}

void DisplayController::sendPopup(POPUP popup,u8 cmd)
{

}

F_RETURN DisplayController::swapPage(u16 pageID)
{
    u32 i =0;
    PAGE page;
    QByteArray imgHeader,imgData, cmdParam;

    imgHeader.resize(18);
    imgHeader[0]  = 0x01;
    imgHeader[1]  = 0x00;
    imgHeader[2]  = 0x00;
    imgHeader[3]  = 0x00;
    imgHeader[4]  = 0x00;
    imgHeader[5]  = 0x00;
    imgHeader[6]  = 0x00;
    imgHeader[7]  = 0xC0;
    imgHeader[8]  = 0x00;
    imgHeader[9]  = 0x90;
    imgHeader[10] = 0x00;
    imgHeader[11] = 0x01;
    //
    imgHeader[12] = 0xC0;
    imgHeader[13] = 0x00;
    imgHeader[14] = 0x90;
    imgHeader[15] = 0x00;
    imgHeader[16] = 0x02;
    imgHeader[17] = 0x00;
    //load page
    imgData.resize(6912);
    imgData.fill(0x55,6912);

    sendDisplayCmd(DPL_CMD_CLEAR_SCREEN,NULL,0);
    //wait for respond

    sleep(1);
    cmdParam.resize(1);
    cmdParam[0] = 1;
    sendDisplayCmd(DPL_CMD_SEND_INFO,cmdParam,1);
    //wait for respond

    sleep(1);

    this->putChar(MAIN_TAG);
    this->putChar(DPL_CMD_CREATE_POPUP);
    this->write(imgHeader);

    this->write(imgData);
    this->putChar('\r');
    this->putChar('\n');
    //sendPopup(page->popup[1],DPL_CMD_CREATE_POPUP);
    return F_SUCCESS;

}

