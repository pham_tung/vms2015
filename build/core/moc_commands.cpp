/****************************************************************************
** Meta object code from reading C++ file 'commands.h'
**
** Created by: The Qt Meta Object Compiler version 67 (Qt 5.4.1)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../../core/devices/leti/commands.h"
#include <QtCore/qbytearray.h>
#include <QtCore/qmetatype.h>
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'commands.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 67
#error "This file was generated using the moc from 5.4.1. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
struct qt_meta_stringdata_letilib__LetiData_t {
    QByteArrayData data[60];
    char stringdata[1014];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_letilib__LetiData_t, stringdata) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_letilib__LetiData_t qt_meta_stringdata_letilib__LetiData = {
    {
QT_MOC_LITERAL(0, 0, 17), // "letilib::LetiData"
QT_MOC_LITERAL(1, 18, 3), // "Tag"
QT_MOC_LITERAL(2, 22, 6), // "SERVER"
QT_MOC_LITERAL(3, 29, 6), // "DEVICE"
QT_MOC_LITERAL(4, 36, 4), // "PING"
QT_MOC_LITERAL(5, 41, 7), // "Command"
QT_MOC_LITERAL(6, 49, 26), // "CMD_SERVER_FONT_UPDATE_REQ"
QT_MOC_LITERAL(7, 76, 16), // "FONT_INFO_HEADER"
QT_MOC_LITERAL(8, 93, 19), // "FONT_PACKAGE_HEADER"
QT_MOC_LITERAL(9, 113, 26), // "CMD_SERVER_DATA_UPDATE_REQ"
QT_MOC_LITERAL(10, 140, 16), // "DATA_INFO_HEADER"
QT_MOC_LITERAL(11, 157, 19), // "DATA_PACKAGE_HEADER"
QT_MOC_LITERAL(12, 177, 30), // "CMD_SERVER_FIRMWARE_UPDATE_REQ"
QT_MOC_LITERAL(13, 208, 20), // "FIRMWARE_INFO_HEADER"
QT_MOC_LITERAL(14, 229, 23), // "FIRMWARE_PACKAGE_HEADER"
QT_MOC_LITERAL(15, 253, 16), // "CMD_INSERT_IMAGE"
QT_MOC_LITERAL(16, 270, 16), // "CMD_UPDATE_IMAGE"
QT_MOC_LITERAL(17, 287, 16), // "CMD_DELETE_IMAGE"
QT_MOC_LITERAL(18, 304, 20), // "CMD_DELETE_ALL_IMAGE"
QT_MOC_LITERAL(19, 325, 24), // "CMD_GET_LIST_IMAGE_INDEX"
QT_MOC_LITERAL(20, 350, 15), // "CMD_CREATE_PAGE"
QT_MOC_LITERAL(21, 366, 15), // "CMD_UPDATE_PAGE"
QT_MOC_LITERAL(22, 382, 15), // "CMD_DELETE_PAGE"
QT_MOC_LITERAL(23, 398, 17), // "CMD_GET_PAGE_INFO"
QT_MOC_LITERAL(24, 416, 19), // "CMD_SET_ACTIVE_PAGE"
QT_MOC_LITERAL(25, 436, 19), // "CMD_GET_ACTIVE_PAGE"
QT_MOC_LITERAL(26, 456, 23), // "CMD_GET_LIST_PAGE_INDEX"
QT_MOC_LITERAL(27, 480, 16), // "CMD_INSERT_POPUP"
QT_MOC_LITERAL(28, 497, 16), // "CMD_UPDATE_POPUP"
QT_MOC_LITERAL(29, 514, 16), // "CMD_DELETE_POPUP"
QT_MOC_LITERAL(30, 531, 30), // "CMD_SERVER_PLAYLIST_UPDATE_REQ"
QT_MOC_LITERAL(31, 562, 20), // "PLAYLIST_INFO_HEADER"
QT_MOC_LITERAL(32, 583, 28), // "CMD_INSERT_IMAGE_TO_PLAYLIST"
QT_MOC_LITERAL(33, 612, 28), // "CMD_UPDATE_IMAGE_TO_PLAYLIST"
QT_MOC_LITERAL(34, 641, 30), // "CMD_DELETE_IMAGE_FROM_PLAYLIST"
QT_MOC_LITERAL(35, 672, 11), // "GET_VERSION"
QT_MOC_LITERAL(36, 684, 12), // "GET_PASSWORD"
QT_MOC_LITERAL(37, 697, 12), // "SET_PASSWORD"
QT_MOC_LITERAL(38, 710, 14), // "GET_BRIGHTNESS"
QT_MOC_LITERAL(39, 725, 14), // "SET_BRIGHTNESS"
QT_MOC_LITERAL(40, 740, 13), // "CMD_CHECK_LED"
QT_MOC_LITERAL(41, 754, 13), // "CMD_CLEAR_LED"
QT_MOC_LITERAL(42, 768, 18), // "CMD_START_SCAN_LED"
QT_MOC_LITERAL(43, 787, 17), // "CMD_STOP_SCAN_LED"
QT_MOC_LITERAL(44, 805, 18), // "CMD_CAPTURE_SCREEN"
QT_MOC_LITERAL(45, 824, 14), // "CMD_GET_STATUS"
QT_MOC_LITERAL(46, 839, 10), // "CMD_REBOOT"
QT_MOC_LITERAL(47, 850, 17), // "CMD_SET_PREDEFINE"
QT_MOC_LITERAL(48, 868, 12), // "CMD_SHUTDOWN"
QT_MOC_LITERAL(49, 881, 12), // "CMD_SET_TIME"
QT_MOC_LITERAL(50, 894, 12), // "CMD_GET_TIME"
QT_MOC_LITERAL(51, 907, 13), // "CMD_SEND_IMEI"
QT_MOC_LITERAL(52, 921, 15), // "LED_INFO_HEADER"
QT_MOC_LITERAL(53, 937, 15), // "LED_SCREEN_DATA"
QT_MOC_LITERAL(54, 953, 15), // "LED_STATUS_DATA"
QT_MOC_LITERAL(55, 969, 7), // "Warning"
QT_MOC_LITERAL(56, 977, 7), // "TOO_HOT"
QT_MOC_LITERAL(57, 985, 7), // "DAMAGED"
QT_MOC_LITERAL(58, 993, 12), // "OUT_OF_POWER"
QT_MOC_LITERAL(59, 1006, 7) // "OPENNED"

    },
    "letilib::LetiData\0Tag\0SERVER\0DEVICE\0"
    "PING\0Command\0CMD_SERVER_FONT_UPDATE_REQ\0"
    "FONT_INFO_HEADER\0FONT_PACKAGE_HEADER\0"
    "CMD_SERVER_DATA_UPDATE_REQ\0DATA_INFO_HEADER\0"
    "DATA_PACKAGE_HEADER\0CMD_SERVER_FIRMWARE_UPDATE_REQ\0"
    "FIRMWARE_INFO_HEADER\0FIRMWARE_PACKAGE_HEADER\0"
    "CMD_INSERT_IMAGE\0CMD_UPDATE_IMAGE\0"
    "CMD_DELETE_IMAGE\0CMD_DELETE_ALL_IMAGE\0"
    "CMD_GET_LIST_IMAGE_INDEX\0CMD_CREATE_PAGE\0"
    "CMD_UPDATE_PAGE\0CMD_DELETE_PAGE\0"
    "CMD_GET_PAGE_INFO\0CMD_SET_ACTIVE_PAGE\0"
    "CMD_GET_ACTIVE_PAGE\0CMD_GET_LIST_PAGE_INDEX\0"
    "CMD_INSERT_POPUP\0CMD_UPDATE_POPUP\0"
    "CMD_DELETE_POPUP\0CMD_SERVER_PLAYLIST_UPDATE_REQ\0"
    "PLAYLIST_INFO_HEADER\0CMD_INSERT_IMAGE_TO_PLAYLIST\0"
    "CMD_UPDATE_IMAGE_TO_PLAYLIST\0"
    "CMD_DELETE_IMAGE_FROM_PLAYLIST\0"
    "GET_VERSION\0GET_PASSWORD\0SET_PASSWORD\0"
    "GET_BRIGHTNESS\0SET_BRIGHTNESS\0"
    "CMD_CHECK_LED\0CMD_CLEAR_LED\0"
    "CMD_START_SCAN_LED\0CMD_STOP_SCAN_LED\0"
    "CMD_CAPTURE_SCREEN\0CMD_GET_STATUS\0"
    "CMD_REBOOT\0CMD_SET_PREDEFINE\0CMD_SHUTDOWN\0"
    "CMD_SET_TIME\0CMD_GET_TIME\0CMD_SEND_IMEI\0"
    "LED_INFO_HEADER\0LED_SCREEN_DATA\0"
    "LED_STATUS_DATA\0Warning\0TOO_HOT\0DAMAGED\0"
    "OUT_OF_POWER\0OPENNED"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_letilib__LetiData[] = {

 // content:
       7,       // revision
       0,       // classname
       0,    0, // classinfo
       0,    0, // methods
       0,    0, // properties
       3,   14, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

 // enums: name, flags, count, data
       1, 0x0,    3,   26,
       5, 0x0,   49,   32,
      55, 0x0,    4,  130,

 // enum data: key, value
       2, uint(letilib::LetiData::SERVER),
       3, uint(letilib::LetiData::DEVICE),
       4, uint(letilib::LetiData::PING),
       6, uint(letilib::LetiData::CMD_SERVER_FONT_UPDATE_REQ),
       7, uint(letilib::LetiData::FONT_INFO_HEADER),
       8, uint(letilib::LetiData::FONT_PACKAGE_HEADER),
       9, uint(letilib::LetiData::CMD_SERVER_DATA_UPDATE_REQ),
      10, uint(letilib::LetiData::DATA_INFO_HEADER),
      11, uint(letilib::LetiData::DATA_PACKAGE_HEADER),
      12, uint(letilib::LetiData::CMD_SERVER_FIRMWARE_UPDATE_REQ),
      13, uint(letilib::LetiData::FIRMWARE_INFO_HEADER),
      14, uint(letilib::LetiData::FIRMWARE_PACKAGE_HEADER),
      15, uint(letilib::LetiData::CMD_INSERT_IMAGE),
      16, uint(letilib::LetiData::CMD_UPDATE_IMAGE),
      17, uint(letilib::LetiData::CMD_DELETE_IMAGE),
      18, uint(letilib::LetiData::CMD_DELETE_ALL_IMAGE),
      19, uint(letilib::LetiData::CMD_GET_LIST_IMAGE_INDEX),
      20, uint(letilib::LetiData::CMD_CREATE_PAGE),
      21, uint(letilib::LetiData::CMD_UPDATE_PAGE),
      22, uint(letilib::LetiData::CMD_DELETE_PAGE),
      23, uint(letilib::LetiData::CMD_GET_PAGE_INFO),
      24, uint(letilib::LetiData::CMD_SET_ACTIVE_PAGE),
      25, uint(letilib::LetiData::CMD_GET_ACTIVE_PAGE),
      26, uint(letilib::LetiData::CMD_GET_LIST_PAGE_INDEX),
      27, uint(letilib::LetiData::CMD_INSERT_POPUP),
      28, uint(letilib::LetiData::CMD_UPDATE_POPUP),
      29, uint(letilib::LetiData::CMD_DELETE_POPUP),
      30, uint(letilib::LetiData::CMD_SERVER_PLAYLIST_UPDATE_REQ),
      31, uint(letilib::LetiData::PLAYLIST_INFO_HEADER),
      32, uint(letilib::LetiData::CMD_INSERT_IMAGE_TO_PLAYLIST),
      33, uint(letilib::LetiData::CMD_UPDATE_IMAGE_TO_PLAYLIST),
      34, uint(letilib::LetiData::CMD_DELETE_IMAGE_FROM_PLAYLIST),
      35, uint(letilib::LetiData::GET_VERSION),
      36, uint(letilib::LetiData::GET_PASSWORD),
      37, uint(letilib::LetiData::SET_PASSWORD),
      38, uint(letilib::LetiData::GET_BRIGHTNESS),
      39, uint(letilib::LetiData::SET_BRIGHTNESS),
      40, uint(letilib::LetiData::CMD_CHECK_LED),
      41, uint(letilib::LetiData::CMD_CLEAR_LED),
      42, uint(letilib::LetiData::CMD_START_SCAN_LED),
      43, uint(letilib::LetiData::CMD_STOP_SCAN_LED),
      44, uint(letilib::LetiData::CMD_CAPTURE_SCREEN),
      45, uint(letilib::LetiData::CMD_GET_STATUS),
      46, uint(letilib::LetiData::CMD_REBOOT),
      47, uint(letilib::LetiData::CMD_SET_PREDEFINE),
      48, uint(letilib::LetiData::CMD_SHUTDOWN),
      49, uint(letilib::LetiData::CMD_SET_TIME),
      50, uint(letilib::LetiData::CMD_GET_TIME),
      51, uint(letilib::LetiData::CMD_SEND_IMEI),
      52, uint(letilib::LetiData::LED_INFO_HEADER),
      53, uint(letilib::LetiData::LED_SCREEN_DATA),
      54, uint(letilib::LetiData::LED_STATUS_DATA),
      56, uint(letilib::LetiData::TOO_HOT),
      57, uint(letilib::LetiData::DAMAGED),
      58, uint(letilib::LetiData::OUT_OF_POWER),
      59, uint(letilib::LetiData::OPENNED),

       0        // eod
};

void letilib::LetiData::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    Q_UNUSED(_o);
    Q_UNUSED(_id);
    Q_UNUSED(_c);
    Q_UNUSED(_a);
}

const QMetaObject letilib::LetiData::staticMetaObject = {
    { &QObject::staticMetaObject, qt_meta_stringdata_letilib__LetiData.data,
      qt_meta_data_letilib__LetiData,  qt_static_metacall, Q_NULLPTR, Q_NULLPTR}
};


const QMetaObject *letilib::LetiData::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *letilib::LetiData::qt_metacast(const char *_clname)
{
    if (!_clname) return Q_NULLPTR;
    if (!strcmp(_clname, qt_meta_stringdata_letilib__LetiData.stringdata))
        return static_cast<void*>(const_cast< LetiData*>(this));
    return QObject::qt_metacast(_clname);
}

int letilib::LetiData::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QObject::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    return _id;
}
QT_END_MOC_NAMESPACE
