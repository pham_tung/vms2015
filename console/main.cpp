#include <QCoreApplication>
#include <QDebug>
#include <devices/leti/device.h>
#include "stdio.h"
#include "unistd.h"
#include <linux/i2c-dev.h>
#include <linux/i2c.h>
#include <sys/ioctl.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
using namespace letilib;
int test_i2c();
int main(int argc, char *argv[])
{
#if TEST_I2c
    test_i2c();
    return 0;
#endif
    QCoreApplication a(argc, argv);
    LetiDevice* device = new LetiDevice();
    qDebug() << device->getID();
    qDebug() << device->getIMEI() << endl;
    qDebug() << "start login";
    device->login();

    //while(1){}
    //return 0;
    a.exec();

    qDebug()<< "quit";
    delete device;
}
/* Functions to do conversions between BCD and decimal */
unsigned int bcd_to_decimal(unsigned int bcd) {
    //return bcd;
    return ((bcd & 0xf0) >> 4) * 10 + (bcd & 0x0f);
}
unsigned int decimal_to_bcd(unsigned int d) {
    return ((d / 10) << 4) + (d % 10);
}

int test_i2c()
{
    int file;
    char filename[40];
    char buf[10];
    int addr = 0x68;        // The I2C address of the RTC

    sprintf(filename,"/dev/i2c-1");
    if ((file = open(filename,O_RDWR)) < 0) {
        printf("Failed to open the bus.");
        /* ERROR HANDLING; you can check errno to see what went wrong */
        exit(1);
    }

    if (ioctl(file,I2C_SLAVE,addr) < 0) {
        printf("Failed to acquire bus access and/or talk to slave.\n");
        /* ERROR HANDLING; you can check errno to see what went wrong */
        exit(1);
    }

    __u8 reg = 0x00;
    buf[0] = reg;
    buf[1] = 0x00;

    if (write(file,buf,2) != 2) {
        /* ERROR HANDLING: i2c transaction failed */
        printf("Failed to write to the i2c bus.\n");
        //buffer = strerror(errno);
        //printf(buffer);
        printf("\n\n");
    }

    for(int i = 0; i<100; i++) {
        buf[0] = reg;
        if (write(file,buf,1) != 1) {
            /* ERROR HANDLING: i2c transaction failed */
            printf("Failed to write to the i2c bus.\n");
            //buffer = strerror(errno);
            //printf(buffer);
            printf("\n\n");
        }
        // Using I2C Read
        if (read(file,buf,7) != 7) {
            /* ERROR HANDLING: i2c transaction failed */
            printf("Failed to read from the i2c bus.\n");
            //buffer = strerror(errno);
            //printf(buffer);
            printf("\n\n");
        } else {
            int tm_sec = bcd_to_decimal(buf[0]);
            int tm_min = bcd_to_decimal(buf[1]);
            int tm_hour = bcd_to_decimal(buf[2]);
            int tm_mday = bcd_to_decimal(buf[3]);
            int tm_wday = bcd_to_decimal(buf[4]);
            int tm_mon = bcd_to_decimal(buf[5]) ;
            int tm_year = bcd_to_decimal(buf[6]);
            qDebug() << tm_sec << tm_min << tm_hour << tm_mday;
        }
        sleep(1);
    }


}
