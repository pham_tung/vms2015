#-------------------------------------------------
#
# Project created by QtCreator 2015-04-09T13:50:40
#
#-------------------------------------------------

QT       += core

QT       -= gui

TARGET = console
CONFIG   += console
CONFIG   -= app_bundle

TEMPLATE = app

SOURCES += main.cpp \
    test_gpio.cpp \
    test_spi.cpp

LIBS += -L$$OUT_PWD/../core/ -lcore

INCLUDEPATH += $$PWD/../core
DEPENDPATH += $$PWD/../core
